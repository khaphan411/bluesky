import React from "react";
import { Router, Route, Switch } from "react-router-dom";
import { createBrowserHistory } from "history";
import { ThemeProvider } from "@material-ui/styles";

import "./assets/scss/material-kit-react.scss?v=1.4.0";
import "./assets/jss/override.css";

import theme from "./components/theme/theme";

import Landing from "./view/landing/Landing";

const hist = createBrowserHistory();

function App() {
  return (
    <ThemeProvider theme={theme}>
      <Router history={hist}>
        <Switch>
          <Route path="/" component={Landing} />
        </Switch>
      </Router>
    </ThemeProvider>
  );
}

export default App;
