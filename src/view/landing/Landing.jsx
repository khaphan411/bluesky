// @flow

import React from "react";
// nodejs library that concatenates classes
import classNames from "classnames";
// react components for routing our app without refresh
// import { Link } from 'react-router-dom';
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
// @material-ui/icons
// core components
import Header from "../../components/navBar/MyNavBar";
import Footer from "../../components/footer/MyFooter";
import HeaderLinks from "../../components/navBar/MyLinks";
import GridContainer from "../../components/grid/MyGridContainer";
import GridItem from "../../components/grid/MyGridItem";
import Parallax from "../../components/parallax/MyParallax";
import FixedParallax from "../../components/parallax/MyFixedParallax";
import Achivements from "../../components/achivements/MyAchivements";
import CustomCarousel from "../../components/carousel/MyCarousel";
import MySmallGallery from "../../components/galleries/MySmallGallery";
// import CustomCarousel from 'components/CustomCarousel/CustomCarousel.jsx';
// sections for this page
import componentsStyle from "../../assets/jss/components";
import zIndex from "@material-ui/core/styles/zIndex";

import image1 from "../../assets/images/landingBG.jpg";
import image2 from "../../assets/images/landingBG.jpg";
import image3 from "../../assets/images/landingBG.jpg";

const sliders = [
  {
    url: image1,
    caption: "hello world"
  },
  {
    url: image2,
    caption: "hello world"
  },
  {
    url: image3,
    caption: "hello world"
  }
];

const Home = ({ classes, ...rest }) => (
  <React.Fragment>
    {/* header zone */}
    <Header
      brand="Blue Sky"
      rightLinks={<HeaderLinks />}
      fixed
      color="transparent"
      changeColorOnScroll={{
        height: 350,
        color: "white"
      }}
      {...rest}
    />
    <CustomCarousel filter src={sliders} />

    {/* <Parallax image={require("../../assets/images/landingBG.jpg")} filter>
        <div className={classes.container} style={{ zIndex: 2 }}>
          <GridContainer>
            <GridItem>
              <div className={classes.brand}>
                <h1 className={classes.title}>Testing.</h1>
                <h3 className={classes.subtitle}>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                  do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                </h3>
              </div>
            </GridItem>
          </GridContainer>
        </div>
      </Parallax> */}

    {/* content zone */}
    <div
      className={classNames(
        classes.main,
        classes.mainRaised,
        classes.mainMobile
      )}
    >
      <Achivements
        items={[
          "THIẾT KẾ KIẾN TRÚC",
          "XÂY NHÀ TRỌN GÓI",
          "XƯỞNG THI CÔNG NỘI THẤT"
        ]}
      />
      <div className={classes.sections}>
        <div className={classes.container + " " + classes.containerMobile}>
          <MySmallGallery
            heading={"CÔNG TRÌNH THIẾT KẾ"}
            gallery={[
              {
                src: require("../../assets/images/landingBG.jpg"),
                caption: "Anh Ky - Penthouse",
                size: "500"
              },
              {
                src: require("../../assets/images/landingBG.jpg"),
                caption: "Anh Phu - Penthousehu",
                size: "500"
              },
              {
                src: require("../../assets/images/landingBG.jpg"),
                caption: "Anh Ky - Penthouse",
                size: "500"
              },
              {
                src: require("../../assets/images/landingBG.jpg"),
                caption: "Anh Phu - Penthousehu",
                size: "500"
              },
              {
                src: require("../../assets/images/landingBG.jpg"),
                caption: "Anh Ky - Penthouse",
                size: "500"
              },
              {
                src: require("../../assets/images/landingBG.jpg"),
                caption: "Anh Phu - Penthousehu",
                size: "500"
              }
            ]}
          />
          <MySmallGallery
            heading={"ẢNH THI CÔNG"}
            gallery={[
              {
                src: require("../../assets/images/landingBG.jpg"),
                caption: "Anh Ky - Penthouse",
                size: "500"
              },
              {
                src: require("../../assets/images/landingBG.jpg"),
                caption: "Anh Phu - Penthousehu",
                size: "500"
              },
              {
                src: require("../../assets/images/landingBG.jpg"),
                caption: "Anh Ky - Penthouse",
                size: "500"
              },
              {
                src: require("../../assets/images/landingBG.jpg"),
                caption: "Anh Phu - Penthousehu",
                size: "500"
              },
              {
                src: require("../../assets/images/landingBG.jpg"),
                caption: "Anh Ky - Penthouse",
                size: "500"
              },
              {
                src: require("../../assets/images/landingBG.jpg"),
                caption: "Anh Phu - Penthousehu",
                size: "500"
              }
            ]}
          />
        </div>
      </div>
      {/* <div className={classes.sections}>
        <div className={classes.container}>
          <GridContainer justify="center" spacing={10}>
            <GridItem className={classes.textCenter}>Hello 1</GridItem>
          </GridContainer>
        </div>
      </div>
      <div className={classes.sections}>
        <div className={classes.container}>
          <GridContainer justify="center">
            <GridItem className={classes.textCenter}>Hello 2</GridItem>
          </GridContainer>
        </div>
      </div> */}
    </div>

    {/* footer zone */}
    <Footer dark />
  </React.Fragment>
);

export default withStyles(componentsStyle)(Home);
