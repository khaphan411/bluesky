import React from "react";
import classNames from "classnames";
import PropTypes from "prop-types";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import Grid from "@material-ui/core/Grid";

const style = theme => ({
  grid: {
    position: "relative",
    width: "100%",
    minHeight: "1px",
    // paddingRight: "15px",
    // paddingLeft: "15px",
    flexBasis: "auto",
  },
  // change direction from column to row when display on mobile
  isMobile: {
    [theme.breakpoints.down("sm")]: {
      display: "-moz-box",
      display: "-webkit-box",
      overflow: "auto",
      padding: "0px",
    },
  },
  isCarousel: {
    [theme.breakpoints.down("sm")]: {
      display: "block",
      overflow: "auto",
      padding: "0px",
    },
  },
});

const GridItem = ({
  classes,
  className,
  children,
  isMobile,
  isCarousel,
  ...rest
}) => {
  const gridClasses = classNames({
    [classes.grid]: true,
    [classes.isMobile]: isMobile,
    [classes.isCarousel]: isCarousel,
  });
  return (
    <Grid item {...rest} className={`${gridClasses} ${className}`}>
      {children}
    </Grid>
  );
};

GridItem.propTypes = {
  classes: PropTypes.object.isRequired,
  isMobile: PropTypes.bool,
};

export default withStyles(style)(GridItem);
