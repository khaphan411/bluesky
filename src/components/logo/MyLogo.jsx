// @flow

import React from "react";
import classNames from "classnames";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Avatar from "@material-ui/core/Avatar";
import image from "../../assets/images/logo.jpg";

const styles = theme => ({
  logoSM: {
    width: 40,
    height: 40,
  },
  logoM: {
    width: 80,
    height: 80,
  },
  logoL: {
    width: 110,
    height: 110,
  },
  marginRight: {
    marginRight: "10px",
    [theme.breakpoints.down("xs")]: {
      marginRight: "0px",
    },
  },
});

const Logo = props => {
  const { classes, sizeSM, sizeM, sizeL, marginRight } = props;
  const logoClasses = classNames({
    [classes.marginRight]: marginRight,
    [classes.logoSM]: sizeSM,
    [classes.logoM]: sizeM,
    [classes.logoL]: sizeL,
  });
  return <Avatar alt="Huflit Logo" src={image} className={logoClasses} />;
};

Logo.propTypes = {
  classes: PropTypes.object.isRequired,
  sizeSM: PropTypes.bool,
  sizeM: PropTypes.bool,
  sizeL: PropTypes.bool,
  marginRight: PropTypes.bool,
};

export default withStyles(styles)(Logo);
