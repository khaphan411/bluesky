//@flow

import React from "react";
import classNames from "classnames";
// nodejs library to set properties for components
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core";
import ButtonBase from "@material-ui/core/ButtonBase";
import Typography from "@material-ui/core/Typography";

import prettyButtonStyle from "../../assets/jss/prettyButtonStyle";

const PrettyButton = ({ classes, src, caption, size, key, ...rest }) => {
  return (
    <div className={classes.root}>
      <ButtonBase
        focusRipple
        key={key}
        className={classes.image}
        focusVisibleClassName={classes.focusVisible}
      >
        <div
          className={classes.imageSrc}
          style={{
            backgroundImage: `url(${src})`,
          }}
        />
        <span className={classes.imageBackdrop} />
        <span className={classes.imageButton}>
          <Typography
            component="span"
            variant="subtitle1"
            color="inherit"
            className={classes.imageTitle}
          >
            {caption + "\n" + size}m<sup>2</sup>
            {/* <span className={classes.imageMarked} /> */}
          </Typography>
        </span>
      </ButtonBase>
    </div>
  );
};

PrettyButton.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(prettyButtonStyle)(PrettyButton);
