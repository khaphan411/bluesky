// @flow

import React from "react";
// nodejs library that concatenates classes
import PropTypes from "prop-types";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
// @material-ui/icons
import Grid from "@material-ui/core/Grid";

const style = theme => ({
  grid: {
    // marginRight: "-15px",
    // marginLeft: "-15px",
    width: "auto",
    [theme.breakpoints.down("sm")]: {
      padding: "0px",
      margin: "0px",
    },
  },
  fixedBGClass: {
    width: "100%",
    minHeight: "360px",
    textAlign: "center",
    margin: "auto",
    overflow: "hidden",
    position: "relative",
    backgroundPosition: "center center",
    backgroundSize: "cover",
    margin: "0",
    padding: "0",
    border: "0",
    display: "flex",
    alignItems: "center",
    [theme.breakpoints.down("sm")]: {
      minHeight: "500px",
    },
    "&:after,&:before": {
      background: "rgba(52, 168, 173, 0.5)",
      position: "absolute",
      zIndex: "1",
      width: "100%",
      height: "100%",
      display: "block",
      left: "0",
      top: "0",
      content: "''",
    },
    // "&:before": {
    //   background: "rgba(0, 227, 234, 0.5)",
    //   color: "#00e3ea",
    //   content: "''",
    //   position: "absolute",
    //   display: "block",
    //   left: "0",
    //   top: "0",
    //   width: "100%",
    //   height: "100%",
    // },
  },
});

function FixedParallax({ ...props }) {
  const { classes, children, className, image, ...rest } = props;
  return (
    <Grid container {...rest} className={classes.grid + " " + className}>
      {children}
      <div
        className={classes.fixedBGClass}
        style={{
          background: image ? `url(${image}) no-repeat top center` : `#38a8ad`,
          backgroundAttachment: "fixed",
        }}
      />
    </Grid>
  );
}

FixedParallax.defaultProps = {
  className: "",
};

FixedParallax.propTypes = {
  classes: PropTypes.object.isRequired,
  children: PropTypes.node,
  className: PropTypes.string,
};

export default withStyles(style)(FixedParallax);
