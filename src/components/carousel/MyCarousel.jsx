import React, { useState } from "react";
// react component for creating beautiful carousel
import Carousel from "react-slick";
// @material-ui/core components
import classNames from "classnames";
import withStyles from "@material-ui/core/styles/withStyles";
// core components
import GridContainer from "../grid/MyGridContainer";
import GridItem from "../grid/MyGridItem";
import Card from "../card/MyCard";
import carouselStyle from "../../assets/jss/carouselStyle.jsx";

const style = theme => ({
  ...carouselStyle,
  headerCarousel: {
    margin: 0,
    padding: 0,
    width: "auto",
    height: "auto",
    borderRadius: 0,
    boxShadow: "none"
  }
});

const CustomCarousel = ({ classes, filter, ...props }) => {
  const slickImageFilter = classNames({
    [classes.filter]: filter
  });

  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    adaptiveHeight: false,
    pauseOnDotsHover: true
    // nextArrow: <ChevronRight style={{ display: "block" }} />,
    // prevArrow: <ChevronLeft style={{ display: "block" }} />,
  };
  const items =
    props.src && props.src.length > 0
      ? props.src.map((image, index) => (
          <div className={slickImageFilter} key={index}>
            <img src={image.url} alt={index} className={classes.slickImage} />
            <div className="slick-caption">
              <h4>{image.caption}</h4>
            </div>
          </div>
        ))
      : undefined;
  return (
    <GridContainer>
      <GridItem isMobile isCarousel>
        <Card carousel className={classes.headerCarousel}>
          <Carousel {...settings}>{items}</Carousel>
        </Card>
      </GridItem>
    </GridContainer>
  );
};

export default withStyles(style)(CustomCarousel);
