//@flow

/*eslint-disable*/
import React from "react";
// nodejs library to set properties for components
import PropTypes from "prop-types";
// nodejs library that concatenates classes
import classNames from "classnames";
import { withStyles } from "@material-ui/core";

import { Typography } from "@material-ui/core";

import GridContainer from "../../components/grid/MyGridContainer";
import FixedParallax from "../../components/parallax/MyFixedParallax";
import GridItem from "../../components/grid/MyGridItem";

import { ThumbUpAlt } from "@material-ui/icons";
import { container } from "../../assets/jss/material-kit-react";

const style = theme => ({
  container,
  containerWide: {
    width: "100%",
    minHeight: "6em",
    background: "#5ac4c9",
    margin: 0,
    alignItems: "center",
  },
  flexCenter: {
    display: "inline-flex",
    justifyContent: "center",
    alignItems: "center",
    minHeight: "50px",
  },
  whiteText: {
    color: "#FFFFFF",
    fontSize: 18,
    fontWeight: "400",
    [theme.breakpoints.down("sm")]: {
      fontSize: 18,
      fontWeight: "400",
    },
    // fontFamily: '"Roboto Slab","Times New Roman", serif',
  },
  azureText: {
    textAlign: "center",
    color: "#ffffff",
    // textShadow: "0px 4px 8px #FF0000",
    padding: "10px",
    margin: "20px 0",
    [theme.breakpoints.down("sm")]: {
      fontSize: 24,
      padding: "10px",
      margin: "20px 0",
    },
    fontFamily: '"Roboto Slab","Times New Roman", serif',
  },
  icon: {
    width: "18px",
    height: "18px",
    color: "#ffc857",
    marginRight: "5px",
  },
  overBackground: {
    position: "absolute",
    zIndex: 2,
    background: "transparent",
    width: "100vw",
  },
});

const Achivements = ({ items, classes, ...props }) => {
  const elements =
    items && items.length > 0
      ? items.map((item, index) => (
          <GridItem
            xs={12}
            sm={12}
            md={4}
            key={index}
            className={classes.flexCenter}
          >
            <ThumbUpAlt className={classes.icon} />
            <Typography variant="subtitle1" className={classes.whiteText}>
              {item}
            </Typography>
          </GridItem>
        ))
      : undefined;
  return (
    <FixedParallax image={require("../../assets/images/landingBG.jpg")}>
      <div className={classes.overBackground}>
        <GridContainer className={classes.containerWide} spacing={5}>
          <GridItem xs={12} className={classes.container}>
            {elements}
          </GridItem>
        </GridContainer>
        <Typography variant="h4" className={classes.azureText}>
          We have everything you need to be happy!
        </Typography>
        {/* <GridContainer
        className={classes.container + " " + classes.overBackground}
      > */}
        {/* </GridContainer> */}
      </div>
    </FixedParallax>
  );
};

Achivements.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(style)(Achivements);
