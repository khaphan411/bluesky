//@flow

/*eslint-disable*/
import React from "react";
// nodejs library to set properties for components
import PropTypes from "prop-types";
// nodejs library that concatenates classes
import classNames from "classnames";
import { withStyles } from "@material-ui/core";

import { Typography } from "@material-ui/core";

import GridContainer from "../../components/grid/MyGridContainer";
import GridItem from "../../components/grid/MyGridItem";
import PrettyButton from "../../components/button/MyPrettyButton";

const style = theme => ({
  grid: {
    padding: 0,
    margin: 0,
    width: "auto",
    [theme.breakpoints.down("sm")]: {
      padding: "0px",
      margin: "0px",
    },
  },
  heading: {
    textAlign: "center",
    padding: "30px",
    marginTop: "30px",
    marginBottom: "30px",
    color: "#1e8e94",
    background:
      "url(" +
      require("../../assets/images/underline.png") +
      ") no-repeat center 70px",
  },
  darkBackground: {
    background: "#393e46",
  },
});

const SmallGallery = ({
  heading,
  gallery,
  dark,
  classes,
  className,
  ...rest
}) => {
  let elements;
  if (gallery && gallery.length > 0) {
    elements = gallery.map((foto, index) => (
      <GridItem key={index} xs={12} sm={12} md={4} key={index}>
        <PrettyButton
          src={foto.src}
          caption={foto.caption.toUpperCase()}
          size={foto.size}
        />
      </GridItem>
    ));
  }
  const galeryClass = classNames({
    [classes.darkBackground]: dark,
  });

  return (
    <div className={galeryClass}>
      <Typography variant="h4" className={classes.heading}>
        {heading}
      </Typography>
      <GridContainer
        {...rest}
        justify="center"
        className={classes.grid + " " + className}
      >
        {elements}
      </GridContainer>
    </div>
  );
};

SmallGallery.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(style)(SmallGallery);
