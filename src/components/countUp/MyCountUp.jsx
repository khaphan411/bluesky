import React, { useState } from "react";
// @material-ui/core components
import classNames from "classnames";
import withStyles from "@material-ui/core/styles/withStyles";

import CountUp from "react-countup";
import { Typography } from "@material-ui/core";

class Parallax extends React.Component {
  constructor(props) {
    super(props);
    var windowScrollTop = window.pageYOffset / 2;
    this.state = {
      animated: false,
    };
    this.activateAnimation = this.activateAnimation.bind(this);
  }
  componentDidMount() {
    var windowScrollTop = window.pageYOffset;
    this.setState({
      animated: false,
    });
    window.addEventListener("scroll", this.resetTransform);
  }
  componentWillUnmount() {
    window.removeEventListener("scroll", this.resetTransform);
  }
  activateAnimation() {
    var windowScrollTop = window.pageYOffset;
    var achivementTop = $(".*fixedBGClass*").getBoundingClientRect().top;
    if (windowScrollTop - achivementTop === 100)
      this.setState({
        animated: true,
      });
  }
  render() {
    const { classes, begin, end ...rest } = this.props;
    return (
      <CountUp>
        <Typography
        component="h1"></Typography>
      </CountUp>
    );
  }
}

Parallax.propTypes = {
  classes: PropTypes.object.isRequired,
  className: PropTypes.string,
  filter: PropTypes.bool,
  children: PropTypes.node,
  style: PropTypes.string,
  image: PropTypes.string,
};

export default withStyles(parallaxStyle)(Parallax);
