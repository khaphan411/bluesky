/*eslint-disable*/
import React from "react";
// react components for routing our app without refresh
import { Link } from "react-router-dom";

// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import Tooltip from "@material-ui/core/Tooltip";

// @material-ui/icons
import { Apps, Person, Notifications } from "@material-ui/icons";

// core components
import Button from "../../components/button/MyButton.jsx";

import headerLinksStyle from "../../assets/jss/headerLinksStyle";

const HeaderLinks = ({ classes }) => (
  <List className={classes.list}>
    <ListItem className={classes.listItem}>
      <Button
        href="/"
        color="transparent"
        target="_blank"
        className={classes.navLink}
        disableRipple
        disableFocusRipple
      >
        Trang chủ
      </Button>
    </ListItem>
    <ListItem className={classes.listItem}>
      <Button
        href="/"
        color="transparent"
        target="_blank"
        className={classes.navLink}
        disableRipple
        disableFocusRipple
      >
        Công trình thiết kế
      </Button>
    </ListItem>
    <ListItem className={classes.listItem}>
      <Button
        href="/"
        color="transparent"
        target="_blank"
        className={classes.navLink}
        disableRipple
        disableFocusRipple
      >
        Ảnh thi công
      </Button>
    </ListItem>
    <ListItem className={classes.listItem}>
      <Button
        href="/"
        color="transparent"
        target="_blank"
        className={classes.navLink}
        disableRipple
        disableFocusRipple
      >
        Xây dựng
      </Button>
    </ListItem>
    <ListItem className={classes.listItem}>
      <Button
        href="/"
        color="transparent"
        target="_blank"
        className={classes.navLink}
        disableRipple
        disableFocusRipple
      >
        Nội thất
      </Button>
    </ListItem>
    <ListItem className={classes.listItem}>
      <Button
        href="/"
        color="transparent"
        target="_blank"
        className={classes.navLink}
        disableRipple
        disableFocusRipple
      >
        Bảng giá
      </Button>
    </ListItem>
    <ListItem className={classes.listItem}>
      <Button
        href="/"
        color="transparent"
        target="_blank"
        className={classes.navLink}
        disableRipple
        disableFocusRipple
      >
        Liên hệ
      </Button>
    </ListItem>
  </List>
);

export default withStyles(headerLinksStyle)(HeaderLinks);
