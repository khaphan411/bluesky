/*eslint-disable*/
import React from "react";
// nodejs library to set properties for components
import PropTypes from "prop-types";
// nodejs library that concatenates classes
import classNames from "classnames";
import { withStyles, Typography } from "@material-ui/core";

import GridContainer from "../../components/grid/MyGridContainer";
import GridItem from "../../components/grid/MyGridItem";

// @material-ui/icons
import Favorite from "@material-ui/icons/Favorite";

import footerStyle from "../../assets/jss/footerStyle";

const Footer = ({ ...props }) => {
  const { classes, dark, whiteFont } = props;
  const footerClasses = classNames({
    [classes.footer]: true,
    [classes.footerWhiteFont]: whiteFont
  });
  const aClasses = classNames({
    [classes.a]: true,
    [classes.footerWhiteFont]: whiteFont
  });
  const darkClass = classNames({
    [classes.darkFooter]: dark
  });
  const darkestClass = classNames({
    [classes.darkestFooter]: dark
  });
  return (
    <React.Fragment>
      <footer className={`${darkClass}`}>
        <div className={classes.container}>
          <GridContainer spacing={5}>
            <GridItem xs={12} sm={12} md={4} className={classes.footerBlock}>
              <span style={{ width: "100%" }}>
                <h5 className={classes.footerHeading}>LIÊN HỆ</h5>
                <div className={classes.underlineAccent} />
                <div className={classes.underline} />
              </span>
              <span
                className={classes.inline}
                style={{
                  marginTop: "10px",
                  fontWeight: "400",
                  fontSize: "1.3rem"
                }}
              >
                HOTLINE:
                <a href="tel:0123456789">
                  <Typography
                    component="p"
                    display="inline"
                    style={{
                      marginLeft: "5px",
                      fontWeight: "400",
                      fontSize: "1.5rem"
                    }}
                  >
                    0123456789
                  </Typography>
                </a>
              </span>
              <span className={classes.inline}>
                EMAIL:
                <a href="email:abc@abc.com">
                  <Typography
                    component="p"
                    display="inline"
                    style={{
                      marginLeft: "5px",
                      fontWeight: "400",
                      fontSize: "1.3rem"
                    }}
                  >
                    abc@abc.com
                  </Typography>
                </a>
              </span>
            </GridItem>
            <GridItem xs={12} sm={12} md={4} className={classes.footerBlock}>
              Divide details about your product or agency work into parts. Write
              a few lines about each one. A paragraph describing a feature will
              be enough.Divide details about your product or agency work into
              parts. Write a few lines about each one. A paragraph describing a
              feature will be enough.Divide details about your product or agency
              work into parts. Write a few lines about each one. A paragraph
              describing a feature will be enough.
            </GridItem>
            <GridItem xs={12} sm={12} md={4} className={classes.footerBlock}>
              Divide details about your product or agency work into parts. Write
              a few lines about each one. A paragraph describing a feature will
              be enough.Divide details about your product or agency work into
              parts. Write a few lines about each one. A paragraph describing a
              feature will be enough.Divide details about your product or agency
              work into parts. Write a few lines about each one. A paragraph
              describing a feature will be enough.
            </GridItem>
          </GridContainer>
        </div>
      </footer>
      <footer className={`${footerClasses} ${darkestClass}`}>
        <div className={classes.container}>
          <div className={classes.center}>
            &copy; {1900 + new Date().getYear()} , made with{" "}
            <Favorite className={classes.icon} /> by{" "}
            <a
              href="https://www.facebook.com/huynh.dohuta"
              className={aClasses}
              target="_blank"
            >
              dohuta
            </a>{" "}
            for my beloved HUFLIT.
          </div>
        </div>
      </footer>
    </React.Fragment>
  );
};

Footer.propTypes = {
  classes: PropTypes.object.isRequired,
  whiteFont: PropTypes.bool
};

export default withStyles(footerStyle)(Footer);
