import { container } from "./material-kit-react.jsx";

const carouselStyle = {
  section: {
    padding: "70px 0",
  },
  height: "100px",
  container,
  marginAuto: {
    marginLeft: "auto !important",
    marginRight: "auto !important",
  },
  filter: {
    // '&:before': {
    //   background: 'rgba(0, 0, 0, 0.5)',
    // },
    // '&:after,&:before': {
    //   position: 'absolute',
    //   zIndex: '1',
    //   width: '100%',
    //   height: '100%',
    //   display: 'block',
    //   left: '0',
    //   top: '0',
    //   content: "''",
    // },
    "&:before": {
      background: "rgba(0, 0, 0, 0.3)",
      color: "#FFFFFF",
      content: "''",
      position: "absolute",
      display: "block",
      left: "0",
      top: "0",
      width: "100%",
      height: "100%",
      zIndex: "1",
    },
  },
  slickImage: {
    width: "100% !important",
    display: "inline-flex !important",
    objectFit: "cover",
    maxHeight: "800px",
    minHeight: "500px",
  },
};

export default carouselStyle;
