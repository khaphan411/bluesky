import { container, primaryColor } from "./material-kit-react.jsx";

const footerStyle = {
  block: {
    color: "inherit",
    padding: "0.9375rem",
    fontWeight: "500",
    fontSize: "12px",
    textTransform: "uppercase",
    borderRadius: "3px",
    textDecoration: "none",
    position: "relative",
    display: "block",
  },
  left: {
    float: "left!important",
    display: "block",
  },
  right: {
    padding: "15px 0",
    margin: "0",
    float: "right!important",
  },
  center: {
    padding: "0",
    margin: "0",
    justifyContent: "center",
    display: "flex-box",
  },
  right: {
    padding: "15px 0",
    margin: "0",
    float: "right!important",
  },
  footer: {
    padding: "0.9375rem 5px",
    textAlign: "center",
    display: "flex",
    zIndex: "2",
    position: "relative",
  },
  darkFooter: {
    padding: "1.5rem 5px",
    background: "#303845",
    color: "#E1E2E4",
  },
  darkestFooter: {
    background: "#222831",
    color: "#E1E2E4",
  },
  a: {
    color: primaryColor,
    textDecoration: "none",
    backgroundColor: "transparent",
  },
  footerWhiteFont: {
    "&,&:hover,&:focus": {
      color: "#FFFFFF",
    },
  },
  container,
  list: {
    marginBottom: "0",
    padding: "0",
    marginTop: "0",
  },
  inlineBlock: {
    display: "inline-block",
    padding: "0px",
    width: "auto",
  },
  icon: {
    width: "18px",
    height: "18px",
    position: "relative",
    top: "3px",
  },
  footerBlock: {
    display: "flex",
    flexWrap: "wrap",
    flexDirection: "column",
    alignItems: "baseline",
  },
  inline: {
    display: "flex",
    alignItems: "center",
  },
  footerHeading: {
    marginBottom: "10px",
    fontWeight: "800",
    fontSize: "1.1rem",
  },
  underlineAccent: {
    height: "1px",
    width: "14%",
    background: "#ffa557",
    position: "absolute",
  },
  underline: {
    left: "22%",
    width: "78%",
    height: "1px",
    background: "#5ac4c9",
    position: "absolute",
  },
};
export default footerStyle;
